package it.unimi.di.vec.ass1;

import static org.assertj.core.api.Assertions.assertThat;
import static org.quicktheories.QuickTheory.qt;
import static org.quicktheories.generators.SourceDSL.arrays;
import static org.quicktheories.generators.SourceDSL.integers;

import java.io.*;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.quicktheories.core.Gen;
import org.quicktheories.core.RandomnessSource;
import org.quicktheories.impl.Constraint;

public class TrianglePBTest {

  @Test
  @Tag("slow")
  void testEquilatero() {
    qt().forAll(integers().allPositive())
        .checkAssert(
            i -> {
              String istr = Integer.toString(i);
              // SETUP
              InputStream in =
                  new ByteArrayInputStream(String.join(" ", istr, istr, istr).getBytes());
              OutputStream out = new ByteArrayOutputStream();

              // EXERCISE
              MyTriangle t = new MyTriangle(in, new PrintStream(out));
              t.describe();

              // VERIFY
              assertThat(out.toString()).isEqualToIgnoringNewLines("equilatero");
            });
  }

  static boolean areValidSides(Integer[] sides) {
      return (sides[0] + sides[1] >= sides[2] && sides[0] + sides[2] >= sides[1] && sides[2] + sides[1] >= sides[0]);
  }

  static boolean areDifferentSides(Integer[] sides) {
    return (!sides[0].equals(sides[2]) && !sides[1].equals(sides[2]) && !sides[0].equals(sides[1]));
  }

  static boolean atLeastTwoDifferentSides(Integer[] sides) {
      return (
              !sides[0].equals(sides[1]) || !sides[0].equals(sides[2]) || !sides[1].equals(sides[2])
              ) && (
              !sides[0].equals(sides[1]) && !sides[0].equals(sides[2]) && !sides[1].equals(sides[2])
      );
  }

  public class ThreeSidesGen implements Gen<Integer[]> {
    @Override
    public Integer[] generate(RandomnessSource in) {
      int a = Math.toIntExact(in.next(Constraint.between(1, Integer.MAX_VALUE)));
      int b = Math.toIntExact(in.next(Constraint.between(1, Integer.MAX_VALUE)));
      int c_max = (long) a + (long) b > Integer.MAX_VALUE ? Integer.MAX_VALUE - 1 : a + b - 1;
      int c = Math.toIntExact(in.next(Constraint.between(Math.abs(a - b), c_max)));
      return new Integer[] {a, b, c};
    }
  }

    public class ThreeSidesGenWithTwoEqual implements Gen<Integer[]> {
        @Override
        public Integer[] generate(RandomnessSource in) {
            int a = Math.toIntExact(in.next(Constraint.between(1, Integer.MAX_VALUE)));
            int c_max = (long) a + (long) a > Integer.MAX_VALUE ? Integer.MAX_VALUE - 1 : a + a - 1;
            int c = Math.toIntExact(in.next(Constraint.between(1, c_max)));
            return new Integer[] {a, a, c};
        }
    }

  @Test
  @Tag("slow")
  void testScaleno() {
    qt().forAll(new ThreeSidesGen())
            .assuming(TrianglePBTest::areDifferentSides)
                .describedAs(
                    (sides) ->
                        String.join(
                            " ",
                            Integer.toString(sides[0]),
                            Integer.toString(sides[1]),
                            Integer.toString(sides[2]), Boolean.toString(areDifferentSides(sides)))
                )
        .checkAssert(
            (sides) -> {
              // SETUP
              InputStream in =
                  new ByteArrayInputStream(
                      String.join(
                              " ",
                              Integer.toString(sides[0]),
                              Integer.toString(sides[1]),
                              Integer.toString(sides[2]))
                          .getBytes());
              OutputStream out = new ByteArrayOutputStream();

              // EXERCISE
              MyTriangle t = new MyTriangle(in, new PrintStream(out));
              t.describe();

              // VERIFY
              assertThat(out.toString()).isEqualToIgnoringNewLines("scaleno");
            });
  }

  @Test
  @Tag("slow")
  void testIsoscele() {
      qt().forAll(new ThreeSidesGenWithTwoEqual())
              .assuming(TrianglePBTest::atLeastTwoDifferentSides)
              .describedAs(
                      (sides) ->
                              String.join(
                                      " ",
                                      Integer.toString(sides[0]),
                                      Integer.toString(sides[1]),
                                      Integer.toString(sides[2]))
              )
              .checkAssert(
                      (sides) -> {
                          // SETUP
                          InputStream in =
                                  new ByteArrayInputStream(
                                          String.join(
                                                  " ",
                                                  Integer.toString(sides[0]),
                                                  Integer.toString(sides[1]),
                                                  Integer.toString(sides[2]))
                                                  .getBytes());
                          OutputStream out = new ByteArrayOutputStream();

                          // EXERCISE
                          MyTriangle t = new MyTriangle(in, new PrintStream(out));
                          t.describe();

                          // VERIFY
                          assertThat(out.toString()).isEqualToIgnoringNewLines("isoscele");
                      });
  }

    @Test
    @Tag("slow")
    void testAnyTriangle() {
        qt().forAll(new ThreeSidesGen())
                .describedAs(
                        (sides) ->
                                String.join(
                                        " ",
                                        Integer.toString(sides[0]),
                                        Integer.toString(sides[1]),
                                        Integer.toString(sides[2]))
                )
                .checkAssert(
                        (sides) -> {
                            // SETUP
                            InputStream in =
                                    new ByteArrayInputStream(
                                            String.join(
                                                    " ",
                                                    Integer.toString(sides[0]),
                                                    Integer.toString(sides[1]),
                                                    Integer.toString(sides[2]))
                                                    .getBytes());
                            OutputStream out = new ByteArrayOutputStream();

                            // EXERCISE
                            MyTriangle t = new MyTriangle(in, new PrintStream(out));
                            t.describe();

                            // VERIFY
                            assertThat(out.toString().strip()).isIn("isoscele", "equilatero", "scaleno");
                        });
    }
}
