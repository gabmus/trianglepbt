package it.unimi.di.vec.ass1;

import static it.unimi.di.vec.ass1.TrianglePBTest.atLeastTwoDifferentSides;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.*;
import net.jqwik.api.*;

public class TrianglePBTestJqwik {
  @Property
  @Tag("jqwik")
  void testEquilatero(@ForAll("positiveInts") int i) {
    String istr = Integer.toString(i);
    // SETUP
    InputStream in = new ByteArrayInputStream(String.join(" ", istr, istr, istr).getBytes());
    OutputStream out = new ByteArrayOutputStream();

    // EXERCISE
    MyTriangle t = new MyTriangle(in, new PrintStream(out));
    t.describe();

    // VERIFY
    assertThat(out.toString()).isEqualToIgnoringNewLines("equilatero");
  }

  @Property
  @Tag("jqwik")
  void testScaleno(
      @ForAll("triangleCompatibleInts") Tuple.Tuple3<Integer, Integer, Integer> tuple) {
    int i = tuple.get1();
    int j = tuple.get2();
    int k = tuple.get3();
    // SETUP
    InputStream in =
        new ByteArrayInputStream(
            String.join(" ", Integer.toString(i), Integer.toString(j), Integer.toString(k))
                .getBytes());
    OutputStream out = new ByteArrayOutputStream();

    // EXERCISE
    MyTriangle t = new MyTriangle(in, new PrintStream(out));
    t.describe();

    // VERIFY
    assertThat(out.toString()).isEqualToIgnoringNewLines("scaleno");
  }

  @Provide
  Arbitrary<Integer> positiveInts() {
    return Arbitraries.integers();
  }

  @Provide
  Arbitrary<Tuple.Tuple3<Integer, Integer, Integer>> triangleCompatibleInts() {
    return Arbitraries.integers()
        .tuple3()
        .filter(
            (t) -> {
              int i = t.get1();
              int j = t.get2();
              int k = t.get3();
              return (i != k && j != k && i != j && i + j >= k && i + k >= j && k + j >= i);
            });
  }

  @Provide
  Arbitrary<Integer[]> isoscelesCompatibleInts() {
    Arbitrary<Integer> a = Arbitraries.integers().between(1, Integer.MAX_VALUE);
    Arbitrary<Integer> c =
            a.flatMap(
                    s1 -> a.flatMap(
                            s2 -> {
                              int c_max = (long)s1 + (long)s2 > Integer.MAX_VALUE ?
                                      Integer.MAX_VALUE - 1
                                      : s1 + s2 - 1;
                              return Arbitraries.integers().between(1, c_max);
                            }
                    )
            );
    return Combinators.combine(a, a, c).as((s1, s2, s3) -> new Integer[] {s1, s2, s3});
  }

  @Provide
  Arbitrary<Integer[]> threeSidesArray() {
    // da qualche parte qui posso mettere .unique() per rendere unici i valori
    Arbitrary<Integer> ints = Arbitraries.integers().between(1, Integer.MAX_VALUE);
    return ints.array(Integer[].class).ofSize(3).filter(TrianglePBTest::areValidSides);
  }

  @Property
  @Tag("jqwik")
  void testIsoscele(
          @ForAll("isoscelesCompatibleInts") Integer[] sides) {
    Assume.that(atLeastTwoDifferentSides(sides));
    int i = sides[0];
    int j = sides[0];
    int k = sides[2];
    // SETUP
    InputStream in =
            new ByteArrayInputStream(
                    String.join(" ", Integer.toString(i), Integer.toString(j), Integer.toString(k))
                            .getBytes());
    OutputStream out = new ByteArrayOutputStream();

    // EXERCISE
    MyTriangle t = new MyTriangle(in, new PrintStream(out));
    t.describe();

    // VERIFY
    assertThat(out.toString()).isEqualToIgnoringNewLines("isoscele");
  }
}
