# TrianglePBT

Attenzione contiene un sottomodulo con la versione di `triangles` con la *dependency injection*. 

Per clonare entrambi (assumendo che accediate con autenticazione SSH):

```
git clone --recurse-submodules git@gitlab.com:verificaEconvalida/TrianglePBT
```

Se usate l'autenticazione HTTPS (ma perché???) dovrebbe essere:

```sh
git clone https://gitlab.com/verificaEconvalida/TrianglePBT.git
cd TrianglePBT
git submodule set-url triangles https://gitlab.com/verificaEconvalida/triangles.git
git submodule init
git submodule update
```
